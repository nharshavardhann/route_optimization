from sklearn.neighbors import DistanceMetric
from math import radians
import pandas as pd
import numpy as np

location = pd.DataFrame({
    'lat':[13.0335,13.0336,13.0330,13.0400,13.0327,13.0338],
                          'lon':[80.2531,80.2505,80.2532,80.2500,80.2507,80.2508],
                          })

dist = DistanceMetric.get_metric('haversine')
location[['lat','lon']].to_numpy()
locations=dist.pairwise(location [['lat','lon']])*6373


from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp

def create_data_model():
    """Stores the data for the problem."""
    data = {}
    data['distance_matrix'] = locations
    data['demands'] = [0, 1, 1, 2, 4, 2]
    data['vehicle_capacities'] = [5, 1, 2,5]
    data['num_vehicles'] = 4
    data['depot'] = 0
    return data
data =create_data_model()

def print_solution(data, manager, routing, solution):
    """Prints solution on console."""
    total_distance = 0
    total_load = 0
    solutions = {}
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
        route_distance = 0
        route_load = 0
        vehicle_solution = []
        while not routing.IsEnd(index):
            node_index = manager.IndexToNode(index)
            vehicle_solution.append(manager.IndexToNode(index))
            route_load += data['demands'][manager.IndexToNode(index)]
            plan_output += ' {0} Load({1}) -> '.format(manager.IndexToNode(index), route_load)
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            route_distance += routing.GetArcCostForVehicle(
                previous_index, index, vehicle_id)
        plan_output += ' {0} Load({1})\n'.format(manager.IndexToNode(index),
                                                 route_load)
        plan_output += 'Distance of the route: {}m\n'.format(route_distance)
        plan_output += 'Load of the route: {}\n'.format(route_load)
        print(plan_output)
        total_distance += route_distance
        total_load += route_load
        solutions[vehicle_id] = vehicle_solution
    print('Total distance of all routes: {}m'.format(total_distance))
    print('Total load of all routes: {}'.format(total_load))
    return solutions

def main():
    """Solve the CVRP problem."""
    # Instantiate the data problem.
    data = create_data_model()

    # Create the routing index manager.
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']),
                                           data['num_vehicles'], data['depot'])

    # Create Routing Model.
    routing = pywrapcp.RoutingModel(manager)


    # Create and register a transit callback.
    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)

    # Define cost of each arc.
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)


    # Add Capacity constraint.
    def demand_callback(from_index):
        """Returns the demand of the node."""
        # Convert from routing variable Index to demands NodeIndex.
        from_node = manager.IndexToNode(from_index)
        return data['demands'][from_node]

    demand_callback_index = routing.RegisterUnaryTransitCallback(
        demand_callback)
    routing.AddDimensionWithVehicleCapacity(
        demand_callback_index,
        0,  # null capacity slack
        data['vehicle_capacities'],  # vehicle maximum capacities
        True,  # start cumul to zero
        'Capacity')

    # Setting first solution heuristic.
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)

    # Solve the problem.
    solution = routing.SolveWithParameters(search_parameters)

    # Print solution on console.
    if solution:
        solution_dict=print_solution(data, manager, routing, solution)
    return solution_dict
if __name__ == '__main__':
    main()

solution_dict =main()
import matplotlib.pyplot as plt
import numpy as np
f, ax = plt.subplots(figsize = [8,6])


coordinates = [
(13.0335, 80.2531),
       (13.0336, 80.2505),
       (13.033 , 80.2532),
       (13.04  , 80.25  ),
       (13.0327, 80.2507),
       (13.0338, 80.2508)]  # location 16

X = np.array([x[0] for x in coordinates])
Y = np.array([x[1] for x in coordinates])
plt.plot(X, Y, 'ko', markersize=8)
plt.plot(X[0], Y[0], 'gX', markersize=30)

for i, txt in enumerate(coordinates):
    plt.text(X[i] , Y[i], f"{i}")

vehicle_colors = ["k","r", "m","b","y"]
for vehicle in solution_dict:
    plt.plot(X[solution_dict[vehicle] + [0]], Y[solution_dict[vehicle] + [0]], f'{vehicle_colors[vehicle]}--')
    

plt.show()

import mplleaflet
plt.figure(figsize=(30,30))
fig = plt.figure()
plt.plot(Y,X)
plt.plot(Y, X, 'ko', markersize=8)
plt.plot(Y[0], X[0], 'gX', markersize=30)
for i, txt in enumerate(coordinates):
    plt.text(Y[i] +5, X[i]+5, f"{i}")

vehicle_colors = ["r", "m","v","y","g"]
for vehicle in solution_dict:
    plt.plot(Y[solution_dict[vehicle] + [0]], X[solution_dict[vehicle] + [0]], f'{vehicle_colors[vehicle]}')
    

mplleaflet.display(fig=fig)